//
//  RIAuthenticationViewController.m
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import "RIAuthenticationViewController.h"
#import "RILoginRegistrationViewController.h"

@implementation RIAuthenticationViewController

- (IBAction)registerAction:(UIButton *)sender {
    [self pushViewControllerWithAuthenticationType:RIAuthenticationTypeRegistration];
}

- (IBAction)loginAction:(UIButton *)sender {
    [self pushViewControllerWithAuthenticationType:RIAuthenticationTypeLogin];
}

- (void)pushViewControllerWithAuthenticationType:(RIAuthenticationType)type {
    RILoginRegistrationViewController *vc = (RILoginRegistrationViewController *) [[UIStoryboard storyboardWithName:@"AuthenticationFlow" bundle:nil] instantiateViewControllerWithIdentifier:@"loginRegistration"];
    vc.type = type;
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end

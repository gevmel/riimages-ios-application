//
//  RILoginRegistrationViewController.h
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RIEnums.h"

@interface RILoginRegistrationViewController : UIViewController

@property (nonatomic) RIAuthenticationType type;

@end

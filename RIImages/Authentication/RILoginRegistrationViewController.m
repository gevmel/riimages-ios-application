//
//  RILoginRegistrationViewController.m
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import "RILoginRegistrationViewController.h"
#import "RINetworking.h"
#import "RIImagesViewController.h"

@interface RILoginRegistrationViewController ()

@property (weak, nonatomic) IBOutlet UIButton *authenticationButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinnerView;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;

@property (nonatomic, copy) void(^makeRequest)(void);

@end

@implementation RILoginRegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.spinnerView.hidesWhenStopped = YES;
    
    [self configAuthentication];
}

- (void)configAuthentication {
    if (self.type == RIAuthenticationTypeLogin) {
        [self.authenticationButton setTitle:@"Login" forState:UIControlStateNormal];
        
        __weak typeof(self) weakself = self;
        self.makeRequest = ^{
            typeof(self) strongSelf = weakself;

            [strongSelf.spinnerView startAnimating];
            [[RINetworking sharedInstance] loginWithParams:@{@"username" : strongSelf.usernameTextField.text} onSuccess:^(id result) {
                [strongSelf.spinnerView stopAnimating];

                [strongSelf presentImagesViewControllerOn:strongSelf];
            } onFailure:^(NSError *error) {
                [strongSelf.spinnerView stopAnimating];

                NSLog(@"%@", error);
            }];
        };
    } else {
        [self.authenticationButton setTitle:@"Register" forState:UIControlStateNormal];
        
        __weak typeof(self) weakself = self;
        self.makeRequest = ^{
            typeof(self) strongSelf = weakself;
            
            [strongSelf.spinnerView startAnimating];
            [[RINetworking sharedInstance] registerWithParams:@{@"username" : strongSelf.usernameTextField.text} onSuccess:^(id result) {
                [strongSelf.spinnerView stopAnimating];
                
                [strongSelf presentImagesViewControllerOn:strongSelf];
            } onFailure:^(NSError *error) {
                [strongSelf.spinnerView stopAnimating];
                
                NSLog(@"%@", error);
            }];
        };
    }
}

- (void)presentImagesViewControllerOn:(UIViewController *)viewController {
    RIImagesViewController *vc = [RIImagesViewController new];
    [viewController.navigationController setViewControllers:@[vc] animated:NO];
}

- (IBAction)authenticationAction:(UIButton *)sender {
    if (self.makeRequest) {
        self.makeRequest();
    }
}

@end

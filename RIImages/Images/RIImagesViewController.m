//
//  RIImagesViewController.m
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/24/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import "RIImagesViewController.h"
#import "RIImageCellNode.h"
#import "RINetworking.h"
#import "RIEnums.h"

#define CELL_HEIGHT 150
#define RPP 10

@interface RIImagesViewController () <ASCollectionDataSource, ASCollectionDelegate>

@property (nonatomic) ASCollectionNode *collectionNode;

@property (nonatomic) CGFloat cellWidth;
@property (nonatomic) RICollectionItemsStyle style;

@property (nonatomic) NSMutableArray<RIImage *> *data;
@property (nonatomic) NSUInteger page;
@property (nonatomic) NSUInteger total;
@property (nonatomic) BOOL shouldGetMoreData;

@end

@implementation RIImagesViewController

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.data = [NSMutableArray new];
        self.page = 1;
        self.total = 100;
        self.shouldGetMoreData = YES;
        self.style = RICollectionItemsStyleList;
        self.cellWidth = [UIScreen mainScreen].bounds.size.width / 3.0 - 1.0;
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    
    [self setupCollection];
}

- (void)setupCollection {
    self.collectionNode = [[ASCollectionNode alloc] initWithCollectionViewLayout:[self configCollectionViewFlowLayout]];
    self.collectionNode.view.alwaysBounceVertical = YES;
    [self.view addSubnode:self.collectionNode];
    self.collectionNode.backgroundColor = [UIColor clearColor];
    self.collectionNode.dataSource = self;
    self.collectionNode.delegate = self;
    [self.collectionNode.view setShowsVerticalScrollIndicator:NO];
    self.collectionNode.leadingScreensForBatching = 1.0;
}

- (UICollectionViewFlowLayout *)configCollectionViewFlowLayout {
    UICollectionViewFlowLayout *flowLayout = [UICollectionViewFlowLayout new];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.minimumInteritemSpacing = 0.f;
    flowLayout.minimumLineSpacing = 0.f;
    
    return flowLayout;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"list_grid"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(changeItemsStyle)];

    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    if (!CGRectEqualToRect(self.collectionNode.view.frame, self.view.bounds)) {
        self.collectionNode.view.frame = self.view.bounds;
    }
}

- (void)changeItemsStyle {
    self.cellWidth = (self.style == RICollectionItemsStyleList) ? self.collectionNode.frame.size.width : ([UIScreen mainScreen].bounds.size.width / 3.0 - 1.0);
    self.style = (self.style == RICollectionItemsStyleList) ? RICollectionItemsStyleGrid : RICollectionItemsStyleList;
    [self.collectionNode reloadData];
}

#pragma mark - ASCollectionDataSource

- (NSInteger)numberOfSectionsInCollectionNode:(ASCollectionNode *)collectionNode {
    return 1;
}

- (NSInteger)collectionNode:(ASCollectionNode *)collectionNode numberOfItemsInSection:(NSInteger)section {
    return self.data.count;
}

- (ASCellNodeBlock)collectionNode:(ASCollectionNode *)collectionNode nodeBlockForItemAtIndexPath:(NSIndexPath *)indexPath {
    return ^{
        RIImage *image = self.data[indexPath.row];
        RIImageCellNode *cellNode = [[RIImageCellNode alloc] initWithImage:image imageViewController:self];
        
        return cellNode;
    };
}

#pragma mark - ASCollectionDelegate

- (ASSizeRange)collectionNode:(ASCollectionNode *)collectionNode constrainedSizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return ASSizeRangeMake(CGSizeMake(self.cellWidth, CELL_HEIGHT));
}

- (void)collectionNode:(ASCollectionNode *)collectionNode willDisplayItemWithNode:(ASCellNode *)node {
    self.shouldGetMoreData = ([collectionNode indexPathForNode:node].row == self.data.count - 1);
}

- (BOOL)shouldBatchFetchForCollectionNode:(ASCollectionNode *)collectionNode {
    return self.shouldGetMoreData && self.total >= self.page * RPP;
}

- (void)collectionNode:(ASCollectionNode *)collectionNode willBeginBatchFetchWithContext:(ASBatchContext *)context {
    [self retrieveNextPageWithCompletion:^(BOOL completion) {
        [context completeBatchFetching:YES];
        self.shouldGetMoreData = NO;
        self.page += 1;
    }];
}

#pragma mark - Networking

- (void)retrieveNextPageWithCompletion:(void (^)(BOOL completion))completion {
    [[RINetworking sharedInstance] getImagesWithPage:self.page params:@{} onSuccess:^(NSArray<RIImage *> *result) {
        if (result.count) {
            self.total = [result[0].total integerValue];
        }
        
        [self.collectionNode performBatchAnimated:YES updates:^{
            [self.data addObjectsFromArray:result];
            NSArray *addedIndexPaths = [self indexPathsForObjects:result];
            [self.collectionNode insertItemsAtIndexPaths:addedIndexPaths];
        } completion:completion];
    } onFailure:nil];
}

- (NSArray *)indexPathsForObjects:(NSArray *)data {
    NSMutableArray *indexPaths = [NSMutableArray array];
    
    for (RIImage *image in data) {
        NSInteger item = [self.data indexOfObject:image];
        [indexPaths addObject:[NSIndexPath indexPathForItem:item inSection:0]];
    }
    
    return indexPaths;
}

- (void)dealloc {
    self.collectionNode = nil;
}

@end

//
//  RIImageDetailsViewController.m
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import <ProgressHUD/ProgressHUD.h>
#import "RIImageDetailsViewController.h"
#import "RIImageDetailsHeaderView.h"
#import "RIImageDetailsTableViewCell.h"
#import "RINetworking.h"
#import "RIUser.h"

#define HEADER_HEIGHT 554
#define CELL_HEIGHT 172
#define COMMENT_VIEW_HEIGHT 50

@interface RIImageDetailsViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *commentTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinnerView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *commentSpinnerView;

@property (nonatomic) RIImage *image;
@property (nonatomic) NSArray<RIImageComment *> *data;

@end

@implementation RIImageDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self retrieveCommentsData];
    
    [self configTableView];
    
    self.spinnerView.hidesWhenStopped = YES;
    self.commentSpinnerView.hidesWhenStopped = YES;
}

- (void)fillDataWithImage:(RIImage *)image {
    self.image = image;
}

- (void)retrieveCommentsData {
    [self.spinnerView startAnimating];
    [[RINetworking sharedInstance] getCommentsWithImageId:[self.image.imageId integerValue] params:nil onSuccess:^(id result) {
        [self.spinnerView stopAnimating];
        
        self.data = result;
        [self.tableView reloadData];
    } onFailure:^(NSError *error) {
        [self.spinnerView stopAnimating];
    }];
}

- (void)configTableView {
    [self.tableView registerNib:[UINib nibWithNibName:@"RIImageDetailsHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"header"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RIImageDetailsTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.allowsSelection = NO;
    [self.tableView setShowsHorizontalScrollIndicator:NO];
    [self.tableView setShowsVerticalScrollIndicator:NO];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, COMMENT_VIEW_HEIGHT, 0);
}

- (IBAction)sendAction:(UIButton *)sender {
    [self.commentSpinnerView startAnimating];
    NSDictionary *params = @{
                             @"creator_id" : [RIUser sharedInstance].userId,
                             @"content" : self.commentTextField.text
                             };
    [[RINetworking sharedInstance] postCommentWithImageId:[self.image.imageId integerValue] params:params onSuccess:^(id result) {
        [self.commentSpinnerView stopAnimating];
        
        [ProgressHUD showSuccess:@"You've successfully added the comment"];
        self.commentTextField.text = @"";
        [self retrieveCommentsData];
    } onFailure:^(NSError *error) {
        [self.commentSpinnerView stopAnimating];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return section == 1 ? self.data.count : 0;
}

- (UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    RIImageDetailsTableViewCell *cell = (RIImageDetailsTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [cell fillDataWithImageComment:self.data[indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return section == 0 ? HEADER_HEIGHT : 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    RIImageDetailsHeaderView *header = (RIImageDetailsHeaderView *) [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"header"];
    [header fillDataWithImage:self.image];
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CELL_HEIGHT;
}

- (void)dealloc {
    self.tableView.dataSource = nil;
    self.tableView.delegate = nil;
}

@end

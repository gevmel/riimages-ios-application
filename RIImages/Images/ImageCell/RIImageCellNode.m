//
//  RIImageCellNode.m
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/24/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import "RIImageCellNode.h"
#import "RIImageDetailsViewController.h"

@interface RIImageCellNode ()

@property (nonatomic) ASNetworkImageNode *imageNode;
@property (nonatomic, weak) RIImagesViewController *imageViewController;

@property (nonatomic) RIImage *image;

@end

@implementation RIImageCellNode

- (instancetype)initWithImage:(RIImage *)image imageViewController:(RIImagesViewController *)imageViewController {
    self = [super init];
    
    if (self) {
        self.image = image;
        self.imageViewController = imageViewController;
        
        self.imageNode = [ASNetworkImageNode new];
        self.imageNode.URL = [NSURL URLWithString:self.image.urlString];
        self.imageNode.contentMode = UIViewContentModeScaleAspectFill;
        [self.imageNode addTarget:self action:@selector(openDetails) forControlEvents:ASControlNodeEventTouchUpInside];
        
        self.automaticallyManagesSubnodes = YES;
    }
    
    return self;
}

- (void)openDetails {
    RIImageDetailsViewController *vc = (RIImageDetailsViewController *) [[UIStoryboard storyboardWithName:@"ImagesFlow" bundle:nil] instantiateViewControllerWithIdentifier:@"imageDetails"];
    [vc fillDataWithImage:self.image];
    
    [self.imageViewController.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Layout

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    return [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(8, 8, 8, 8) child:self.imageNode];
}

@end

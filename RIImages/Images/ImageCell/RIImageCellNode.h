//
//  RIImageCellNode.h
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/24/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "RIImage.h"
#import "RIImagesViewController.h"

@interface RIImageCellNode : ASCellNode

- (instancetype)initWithImage:(RIImage *)image imageViewController:(RIImagesViewController *)imageViewController;

@end

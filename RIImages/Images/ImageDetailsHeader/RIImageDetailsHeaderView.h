//
//  RIImageDetailsHeaderView.h
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RIImage.h"

@interface RIImageDetailsHeaderView : UITableViewHeaderFooterView

- (void)fillDataWithImage:(RIImage *)image;

@end

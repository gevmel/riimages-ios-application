//
//  RIImageDetailsHeaderView.m
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import "RIImageDetailsHeaderView.h"

@interface RIImageDetailsHeaderView ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@end

@implementation RIImageDetailsHeaderView

- (void)fillDataWithImage:(RIImage *)image {
    [self downloadImageWithUrl:[NSURL URLWithString:image.urlString] completion:^(UIImage *image) {
        self.imageView.image = image;
    }];
    self.titleLabel.text = image.title;
    self.descriptionTextView.text = image.descriptionString;
}

- (void)downloadImageWithUrl:(NSURL *)url completion:(void (^)(UIImage *))completion {
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData *data = [[NSData alloc] initWithContentsOfURL:url];
        if (!data) {
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion([UIImage imageWithData:data]);
        });
    });
}

@end

//
//  RIImageDetailsTableViewCell.h
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RIImageComment.h"

@interface RIImageDetailsTableViewCell : UITableViewCell

- (void)fillDataWithImageComment:(RIImageComment *)imageComment;

@end

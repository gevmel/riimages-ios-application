//
//  RIImageDetailsTableViewCell.m
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import "RIImageDetailsTableViewCell.h"

@interface RIImageDetailsTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;

@end

@implementation RIImageDetailsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
}

- (void)fillDataWithImageComment:(RIImageComment *)imageComment {
    self.usernameLabel.text = imageComment.creator.username;
    self.contentTextView.text = imageComment.content;
}

@end

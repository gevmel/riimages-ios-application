//
//  RIEnums.h
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#ifndef RIEnums_h
#define RIEnums_h

typedef enum : NSUInteger {
    RIAuthenticationTypeNone,
    RIAuthenticationTypeLogin,
    RIAuthenticationTypeRegistration
} RIAuthenticationType;

typedef enum : NSUInteger {
    RICollectionItemsStyleNone,
    RICollectionItemsStyleGrid,
    RICollectionItemsStyleList
} RICollectionItemsStyle;

#endif /* RIEnums_h */

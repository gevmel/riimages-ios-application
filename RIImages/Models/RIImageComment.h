//
//  RIImageComment.h
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "RIUser.h"

@interface RIImageComment : MTLModel <MTLJSONSerializing>

@property (nonatomic) NSString *commentId;
@property (nonatomic) NSString *imageId;
@property (nonatomic) NSString *creatorId;
@property (nonatomic) NSString *content;
@property (nonatomic) RIUser *creator;

@end

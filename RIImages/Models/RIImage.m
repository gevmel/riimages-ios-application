//
//  RIImage.m
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import "RIImage.h"

@implementation RIImage

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"imageId" : @"id",
             @"urlString" : @"url",
             @"originalUrlString" : @"original_url",
             @"title" : @"title",
             @"descriptionString" : @"description",
             @"total" : @"total"
             };
}

+ (NSValueTransformer *)imageIdJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return [value stringValue];
        }
        return value;
    }];
}

+ (NSValueTransformer *)totalJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return [value stringValue];
        }
        return value;
    }];
}

@end

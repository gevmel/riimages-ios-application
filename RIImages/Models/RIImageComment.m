//
//  RIImageComment.m
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import "RIImageComment.h"

@implementation RIImageComment

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"commentId" : @"id",
             @"imageId" : @"image_id",
             @"creatorId" : @"creator_id",
             @"content" : @"content",
             @"creator":@"creator"
             };
}

+ (NSValueTransformer *)commentIdJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return [value stringValue];
        }
        return value;
    }];
}

+ (NSValueTransformer *)imageIdJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return [value stringValue];
        }
        return value;
    }];
}

+ (NSValueTransformer *)creatorIdJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return [value stringValue];
        }
        return value;
    }];
}

+ (NSValueTransformer *)creatorJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        return [MTLJSONAdapter modelOfClass:[RIUser class] fromJSONDictionary:value error:NULL];
    }];
}

@end

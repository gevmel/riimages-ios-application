//
//  RIImage.h
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface RIImage : MTLModel <MTLJSONSerializing>

@property (nonatomic) NSString *imageId;
@property (nonatomic) NSString *urlString;
@property (nonatomic) NSString *originalUrlString;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *descriptionString;
@property (nonatomic) NSString *total;

@end

//
//  RIUser.m
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import "RIUser.h"

static RIUser *sharedInstance = nil;
static NSString *token = nil;

@implementation RIUser

+ (instancetype)sharedInstance {
    return sharedInstance;
}

+ (NSString *)getToken {
    return token;
}

+ (void)setToken:(NSString *)token {
    token = token;
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"userId" : @"id",
             @"username" : @"username",
            };
}

+ (NSValueTransformer *)userIdJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        if ([value isKindOfClass:[NSNumber class]]) {
            return [value stringValue];
        }
        return value;
    }];
}

+ (void)loadUser {
    NSError *error;
    sharedInstance = [MTLJSONAdapter modelOfClass:self.class fromJSONDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"user"] error:&error];
    token = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_token"];
}

@end

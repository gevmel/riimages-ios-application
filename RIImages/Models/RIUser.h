//
//  RIUser.h
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface RIUser : MTLModel <MTLJSONSerializing>

@property (nonatomic) NSString *userId;
@property (nonatomic) NSString *username;

+ (instancetype)sharedInstance;
+ (NSString *)getToken;
+ (void)setToken:(NSString *)token;
+ (void)loadUser;

@end

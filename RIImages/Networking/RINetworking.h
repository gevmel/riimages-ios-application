//
//  RINetworking.h
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

#define BASE_URL @"http://riimages.loc/api/"

@interface RINetworking : NSObject

+ (instancetype)sharedInstance;

- (void)registerWithParams:(NSDictionary *)params onSuccess:(void (^)(id))success onFailure:(void (^)(NSError *))failure;
- (void)loginWithParams:(NSDictionary *)params onSuccess:(void (^)(id))success onFailure:(void (^)(NSError *))failure;
- (void)getImagesWithPage:(NSUInteger)page params:(NSDictionary *)params onSuccess:(void (^)(id))success onFailure:(void (^)(NSError *))failure;
- (void)getCommentsWithImageId:(NSUInteger)imageId params:(NSDictionary *)params onSuccess:(void (^)(id))success onFailure:(void (^)(NSError *))failure;
- (void)postCommentWithImageId:(NSUInteger)imageId params:(NSDictionary *)params onSuccess:(void (^)(id))success onFailure:(void (^)(NSError *))failure;

@end

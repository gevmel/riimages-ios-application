//
//  RINetworking.m
//  RIImages
//
//  Created by Gevorg Melkumyan on 6/23/18.
//  Copyright © 2018 GM. All rights reserved.
//

#import <ProgressHUD/ProgressHUD.h>
#import "RINetworking.h"
#import "RIUser.h"
#import "RIImage.h"
#import "RIImageComment.h"

@interface RINetworking ()

@property (nonatomic) AFURLSessionManager *sessionManager;
@property (nonatomic) AFHTTPSessionManager *httpSessionManager;

@end

@implementation RINetworking

+ (instancetype)sharedInstance {
    static RINetworking *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [self new];
    });
    
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.sessionManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        self.httpSessionManager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    }
    
    return self;
}

- (void)requestToPath:(NSString *)path method:(NSString *)method params:(NSDictionary *)params completionHandler:(void (^)(NSURLResponse *response, id responseObject, NSError *error))completionHandler {
    if (!path) {
        return;
    }
    
    if ([RIUser getToken]) {
        if (![method isEqualToString:@"GET"]) {
            path = [path stringByAppendingString:[NSString stringWithFormat:@"?auth_token=%@", [RIUser getToken]]];
        } else {
            if (params) {
                NSMutableDictionary *mutParams = [params mutableCopy];
                [mutParams addEntriesFromDictionary:@{@"auth_token" : [RIUser getToken]}];
                params = [mutParams copy];
            } else {
                params = @{@"auth_token" : [RIUser getToken]};
            }
        }
    }
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:method URLString:[BASE_URL stringByAppendingString:path] parameters:params error:NULL];
    
    NSURLSessionDataTask *dataTask = [self.sessionManager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        completionHandler(response, responseObject, error);
        if (error) {
            [self handleErrorWithResponseObject:responseObject error:error];
        }
    }];
    [dataTask resume];
}

- (void)handleErrorWithResponseObject:(id)response error:(NSError *)error {
    if ([response isKindOfClass:[NSDictionary class]] && [response valueForKey:@"message"] && [response[@"message"] isKindOfClass:[NSString class]]) {
        [ProgressHUD showError:response[@"message"]];
    } else {
        [ProgressHUD showError:error.localizedDescription];
    }
}

#pragma mark - Requests

- (void)registerWithParams:(NSDictionary *)params onSuccess:(void (^)(id))success onFailure:(void (^)(NSError *))failure {
    [self requestToPath:@"auth/register" method:@"POST" params:params completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (!error) {
            [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"user"] forKey:@"user"];
            [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"user"][@"token"] forKey:@"user_token"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [RIUser loadUser];
            
            if(success) {
                success([RIUser sharedInstance]);
            }
        } else {
            if (failure) {
                failure(error);
            }
        }
    }];
}

- (void)loginWithParams:(NSDictionary *)params onSuccess:(void (^)(id))success onFailure:(void (^)(NSError *))failure {
    [self requestToPath:@"auth/login" method:@"POST" params:params completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (!error) {
            [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"user"] forKey:@"user"];
            [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"user"][@"token"] forKey:@"user_token"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [RIUser loadUser];
            
            if(success) {
                success([RIUser sharedInstance]);
            }
        } else {
            if (failure) {
                failure(error);
            }
        }
    }];
}

- (void)getImagesWithPage:(NSUInteger)page params:(NSDictionary *)params onSuccess:(void (^)(id))success onFailure:(void (^)(NSError *))failure {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:params];
    [dictionary setValue:@(page) forKey:@"page"];
    [self requestToPath:@"images" method:@"GET" params:dictionary completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (!error) {
            if (success) {
                NSArray *images = [MTLJSONAdapter modelsOfClass:[RIImage class] fromJSONArray:responseObject[@"data"] error:nil];
                success(images);
            }
        } else {
            if (failure) {
                failure(error);
            }
        }
    }];
}

- (void)getCommentsWithImageId:(NSUInteger)imageId params:(NSDictionary *)params onSuccess:(void (^)(id))success onFailure:(void (^)(NSError *))failure {
    [self requestToPath:[NSString stringWithFormat:@"images/%@/comments", @(imageId)] method:@"GET" params:params completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (!error) {
            if (success) {
                NSArray *images = [MTLJSONAdapter modelsOfClass:[RIImageComment class] fromJSONArray:responseObject[@"comments"][@"data"] error:nil];
                success(images);
            }
        } else {
            if (failure) {
                failure(error);
            }
        }
    }];
}

- (void)postCommentWithImageId:(NSUInteger)imageId params:(NSDictionary *)params onSuccess:(void (^)(id))success onFailure:(void (^)(NSError *))failure {
    [self requestToPath:[NSString stringWithFormat:@"images/%@/comments", @(imageId)] method:@"POST" params:params completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (!error) {
            if (success) {
                success(responseObject);
            }
        } else {
            if (failure) {
                failure(error);
            }
        }
    }];
}

@end
